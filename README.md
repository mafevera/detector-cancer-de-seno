# Clasificador para la detección de cancer de seno
<img src="/imgs/banner.JPG" style="width:1000px;">
<b>Presentado por:</b><br/>
Maria Fernanda Vera Negrón<br/>
Andrea Fabiana Villamizar Ruiz<br/>
Anngy Nathalia Gomez Avila<br/>

<b>Asignatura:</b><br/>
Inteligencia Artificial

Ingeniería de Sistemas.<br/>
Universidad Industrial de Santander

---
[1] El notebook del proyecto funcional se encuentra en el siguiente enlace:<br/>
https://gitlab.com/mafevera/detector-cancer-de-seno/-/blob/master/notebooks/DetectorCancerDeMama.ipynb

[2] Informe del proyecto: <br/>
https://gitlab.com/mafevera/detector-cancer-de-seno/-/blob/master/ClasificadorDetecci_nC_ncerSeno.pdf

[3] Video del proyecto:<br/>
https://drive.google.com/open?id=1jUk_VxL4p2XUD3uskGgF69QvfJRjN4Jv

